import Vue from "vue";
// import App from "./App.vue";
import {
  LayoutMain
} from './views/layouts';

import router from "@/router";
import store from "./store";
import axios from "axios";
import VueLodash from 'vue-lodash';
const options = {
  name: 'lodash'
};
Vue.use(VueLodash, options);
// import 'ant-design-vue/dist/antd.css';
import './assets/css/ant-theme-custrom.css';
import Antd from 'ant-design-vue';
Vue.config.productionTip = false;
Vue.use(Antd);
new Vue({
  router,
  store,
  render: h => h(LayoutMain)
  // eslint-disable-next-line prettier/prettier
}).$mount("#app");