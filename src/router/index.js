/* eslint-disable prettier/prettier */
import Vue from "vue";
import Router from "vue-router";
import Home from "@/views/pages/Home.vue";
import {
    MemberUpload
} from "@/views/pages";
Vue.use(Router);

export default new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [{
            path: "/",
            name: "home",
            component: Home
        },

    ]
});